console.log("Hello World!");

//Assignment Operators

//Basic Assignment Operator
let assignmentNumber = 8;

// Addition Assignment Operator
assignmentNumber = assignmentNumber + 2;
console.log(assignmentNumber); //10

//Shorthand
assignmentNumber += 2;
console.log(assignmentNumber); //12

assignmentNumber -= 2;
console.log(assignmentNumber); //10

assignmentNumber *= 2;
console.log(assignmentNumber); //20

assignmentNumber /= 2;
console.log(assignmentNumber); //10


//Arithmetic Operators
let x = 1397;
let y = 7831;

//Addition Operator (+)
let sum = x + y;
console.log(sum);

//Subtraction Operator (-)
let difference = x - y;
console.log(difference);

//Multiplication Operator (*)
let product = x * y;
console.log(product);

//Division Operator (/)
let quotient = y / x;
console.log(quotient);

//Modulo Operator 
let remainder = y % x;
console.log(remainder);

//Multiple Operators (PEMDAS rule)
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas); //0.6000000000000001

/*
	1. 3*4
	2. 12/5
	3. 1+2
	4. 3 - 0.6
*/

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas) //0.19999999999999996

pemdas = (1 + (2-3) * (4/5));
console.log(pemdas);


//Increment and Decrement

let z = 1;
//Pre-increment
let increment = ++z;
console.log(increment); //2
console.log(z) //2
//Post-increment
increment = z++;
console.log(increment)// 2
console.log(z) //3

//Pre-decrement
let decrement = --z;
console.log(decrement); //2
console.log(z) //2 

//Post-decrement
decrement = z--;
console.log(decrement); //2
console.log(z); // 1

//Type Coercion

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numC = 16;
let numD = 14;
let nonCoercion = numC + numD;
console.log(nonCoercion);
console.log(typeof nonCoercion);

//Adding Boolean and Number
//true = 1
// false = 0
let numE = true + 1;
console.log(numE);
console.log(typeof numE)

let numF = false + 1;
console.log(numF);

//Comparison Operator

let juan = 'juan';

//Equality Operator (==)
	//~checks whether the operands are equal or have the same content
console.log(1 == 1); // true
console.log(1 == 2); //false
console.log(1 == '1'); // true
console.log(0 == false); //true
console.log(juan == 'juan'); //true

//Inequality Operator (!=)
	//~checks whether the operands are NOT equal or have the different content
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(juan != 'juan'); //false

//Strict Equality operator (===)
	//~checks whether the operands are equal or have the same content, and compares the data type of 2 values

console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === "1"); //false
console.log(0 === false); //false
console.log(juan === 'juan'); //true

//Strict Inequality Operator ~checks whether the operands are NOT equal or have
//the different content  and compares the data type of 2 values

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true
console.log(0 !== false); //true
console.log(juan !== 'juan'); //false

//Logical Operator

let isLegalAge = true;
let isRegistered = false;

//Logical And operator (&&)
	//true && true = true
	//true && false = false

let allRequirementsMet = isLegalAge && isRegistered
console.log(allRequirementsMet);

//Logical OR Operator ( || - Double Pipe)
	//true || true = true
	//true || false = true

let someRequirementsMet = isLegalAge || isRegistered;
console.log(someRequirementsMet); //true

//Logical NOT Operator (! - Exclamation Point)
	//returns opposite value
let someRequirementsNOTMet = !isRegistered;
console.log(someRequirementsNOTMet);

/*Control Structures*/
	/*if else statement*/
	/*switch statement*/

// if .. else statement
	/*
		synthax:

			if(condition) {
				statement
			}
	
	*/

//if statement
	//can stand alone



let numG = -1

if (numG < 0){
	console.log("Hello")
}

if(false == "1") {
	console.log("Will not be printed")
}

//else if clause

let numH = 1;

if (numG > 0) {
	console.log("Hello");
} else if (numH > 0) {
	console.log("World")
}


//else

if (numG > 0) {
	console.log("Hello");
} else if (numH == 0) {
	console.log("World")
} else {
	console.log("Again")
}

let age = 20;
if (age <= 18) {
	console.log("Not Allowed to drink")
} else {
	console.log("Shot na!")
}

/* Mini Activity
 	

*/

let height = 160
if (height < 150) {
	console.log("Did not passed the minimum height requirement")
} else {
	console.log("Passed the minimum height requirement")
}


 // if,else if , else statement with FUNCTIONS

 let message = 'No message';
 console.log(message);

 function determineTyphoonIntensity(windSpeed) {

	if (windSpeed < 30) {
		return 'Not a Typhoon yet'
	} else if (windSpeed <= 61){
		return 'Tropical depression detected'
	} else if (windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical Storm detected';
	} else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe Tropical storm detected';
	} else {
		return 'Typhoon detected'
	}

 }

message = determineTyphoonIntensity(70)
console.log(message);

if(message == 'Tropical Storm detected'){
	console.warn(message);
}

//Truthy and Falsy
	//In JS, a truthy value is a value that is considered true when encountered in a boolean context
	//Valuues are considered true unless defined otherwise
	//Falsy values/exceptions for truthy
		//1.false
		//2. 0
		//3. -0
		//4. ""
		//5. null
		//6. undefined
		//7. NaN

	if (true) {
		console.log("Truthy")
	}

	if (1){
		console.log("Truthy")
	}

	if ([]){
		console.log("Truthy")
	}

	if (false) {
		console.log("Falsy")
	}

//Conditional Ternary Operator
	//The conditional ternary operator takes in three operands
		/*
			1. condition
			2. expression to execute if the condition is true
			3. expression to execute if the condition is false

			Synthax:
			(condition) ? ifTrue : isFalse


		*/

	let ternaryResult = (1 + 18) ? true : false 
	console.log(ternaryResult);


	let name;

	function isofLegalAge(){
		name = 'John';
		return 'You are of legal age limit';
	}

	function isUnderAge(){
		name = 'Jane';
		return 'You are under the age limit';
	}

	let age1 = parseInt(prompt("What is your age?"));
	console.log(typeof age1);

	let legalAge = (age1 > 18) ? isofLegalAge() : isUnderAge();
	console.log(legalAge + ' '+ name);	


//Switch Statement

/*
		Synthax:
		switch(expression){
			case value1:
				statement
				break;
			case value2:
				statement
				break;
			default:
				statement
				break;
		}

*/

let day = prompt("What day of the week is it today?") .toLowerCase();
	console.log(day)

	switch(day){
		case 'monday':
			console.log("The color of the day is red");
			break;
		case 'tuesday':
			console.log("The color of the day is orange");
			break;
		case 'wednesday':
			console.log("The color of the day is blue");
			break;
		case 'thursday':
			console.log("The color of the day is green");
			break;
		case 'friday':
			console.log("The color of the day is yellow");
			break;
		case 'saturday':
			console.log("The color of the day is pink");
			break;
		case 'saturday':
			console.log("The color of the day is violet");
			break;
		default:
			console.log("Please input a valid day")
			break;
	}
	
//Try-Catch-Finally Statement
	function showIntensityAlert (windSpeed){

		try {
			//attempts to execute a code
			alert(determineTyphoonIntensity(windSpeed))
		}
		catch(error) {
			//handle the error
			console.warn(error.message)
		}
		finally {
			//regardless of the result, this will execute
			alert('Intensity updates will show new alert')
		}
	}

	showIntensityAlert(56)








































